#include <pcl/io/pcd_io.h>
#include <stdio.h>
#include <chrono>
#include <thread>
#include <analog2dist.h>
#include <cmath>

#define _USE_MATH_DEFINES

int main()
{

  // Initialize point cloud
  pcl::PointCloud<pcl::PointXYZ> cloud;
  cloud.height = 61;
  cloud.width = 61;
  cloud.is_dense = true;
  cloud.points.resize(cloud.width * cloud.height);

  // The index used when inserting points into the point cloud
  auto i = 0;

  char serialPortFileName[] = "/dev/ttyACM0";
  auto serPort = fopen(serialPortFileName, "a+");

  // Check if the serial port is available
  if (serPort == NULL)
    {
      printf("Nothing connected to %s\n", serialPortFileName);
      return 1;
    }

  auto analogR (new int);

  // Iterate through the array of servo positions
  for (auto polar = 60; polar <= 120; polar+=1)
    {
      for (auto azimuth = -30; azimuth <= 30; azimuth+= 1)
        {
          // Send the scanner position
          fprintf(serPort, "%d,%d\n", polar, azimuth + 90);

          // Wait for the scanner to scan
          std::this_thread::sleep_for(std::chrono::milliseconds(100));

          *analogR = 0;

          // Read the analog reading
          fscanf(serPort, "%d\n", analogR);

          // Insert new point into point cloud
          double dist = analog2dist(*analogR);

          const auto radPolar = polar * M_PI / 180;
          const auto radAzim = azimuth * M_PI / 180;
          cloud.points[i].x = dist * sin(radPolar) * cos(radAzim);
          cloud.points[i].y = dist * sin(radPolar) * sin(radAzim);
          cloud.points[i].z = dist * cos(radPolar);
          i++;
        }
    }

  fclose(serPort);
  pcl::io::savePCDFileASCII("test.pcd", cloud);
}
