cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(uscan-lib)

set (CALLIB_M 172.7341746362043)
set (CALLIB_B 0.008094774617596666)

configure_file (
  "${PROJECT_SOURCE_DIR}/analog2dist.h.in"
  "${PROJECT_SOURCE_DIR}/analog2dist.h"
  )

add_library(uscan-lib analog2dist.cpp)
