#include "analog2dist.h"

double analog2dist(int a)
{
  return CALLIB_M / double(a) + CALLIB_B;
}
