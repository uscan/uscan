#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/cloud_viewer.h>

/**
 * Visualizer for the output point cloud.
 * Simply run this and the GUI opens.
 */
int main ()
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::io::loadPCDFile("test.pcd", *cloud);

  pcl::visualization::CloudViewer viewer("Cloud Viewer");

  viewer.showCloud(cloud);

  while (!viewer.wasStopped());
}
